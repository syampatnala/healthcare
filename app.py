from flask import Flask, render_template,url_for,request,jsonify
import snowflake.connector

app=Flask(__name__)

# Function for connect with Snowflake
def snowCon(wh,db,sc):
	ctx = snowflake.connector.connect(
	    user='patnala_user',
	    password='Hr123*',
	    account='ar34813.ap-south-1.aws',
	    warehouse=wh,
	    database=db,
	    schema=sc,
	    )
	return ctx.cursor()

@app.route('/',methods=['POST','GET'])
def index():
	one_row=[]
	cs = snowCon('compute_wh','raw_datalake','symptoms') #-->connecting to symptoms schema
	if request.method=='POST':
		v=request.form['name']
		ar=v.split(",")
		ar.sort()
		v=""
		for i in set(ar):
			v+='%'+i
		print("-----||||||||||------->",v)
		sy="select distinct symptom_1 from diseases;" #------->Retrieveing symptoms
		cs.execute(sy)
		ss=cs.fetchall()
		if len(set(ar))<0:
			return render_template('index.html',error="please enter minimum of 3 valid syptoms",symp=ss)
		q="select distinct disease,symptom_1 from diseases where symptom_1 ILIKE'"+v+"%';" #-->Retrieving for given symptoms
		
		try:
		    # cs.execute("INSERT INTO sample_python VALUES('syam',18);") #optional
		    # cs.execute("INSERT INTO sample_python VALUES('unika',20);") #optional
		    cs.execute(q)
		    data=[]
		    one_row = cs.fetchall()
		    #-----------------------------For count-----------------------
		    for i in range(len(one_row)):
		    	sp=str(one_row[i][1]).split(",")
		    	data.append(len(set(ar).intersection(set(sp))))
		    print(data)
		    # ------------------------------------------------------------
		    return render_template('index.html',users=one_row,cnt=data,heads=['Disease','Actual Symptoms','Matched Symptoms'],symp=ss)
		finally:
		    cs.close()
		return render_template('index.html',error="diseases not found",symp=ss)
	else:
		sy="select distinct symptom_1 from diseases;"
		cs.execute(sy)
		ss=cs.fetchall()
		return render_template('index.html',error="diseases not found",symp=ss)
	# return render_template('index.html',error="please search for users with name")

@app.route('/demo/<string:hsp>')
def second(hsp):
	css=snowCon('compute_wh','raw_datalake','hospital_data') #----->connecting to hospital_data schema
	q="SELECT * FROM hospital;" # WHERE name='"+str(hsp)+"';"
	css.execute(q)
	sch=css.fetchone()
	css.close()
	return "hai! "+hsp+" school is "+sch[1]
@app.route('/map')
def maps():
	css=snowCon('compute_wh','raw_datalake','hospital_data') #----->connecting to hospital_data schema
	q="SELECT TOP 10 health_facility_name,Latitude,longitude FROM hospital;" # WHERE name='"+str(hsp)+"';"
	css.execute(q)
	sch=css.fetchall()
	css.close()
	return render_template('maps.html',hosp=sch)

@app.route('/dsc',methods=['GET'])
def dsc():
	cs = snowCon('compute_wh','raw_datalake','symptoms')
	sy="select distinct symptom_1 from diseases;"
	cs.execute(sy)
	ss=cs.fetchall()
	return jsonify(ss)

'''@app.route('/pdsc',methods=['POST'])
def postDsc():
	if method=='POST':
		'''

#TO CLOSE CONNECTION
# ctx.close()

if __name__=="__main__":
	app.run(debug=True)